# PlayerLocator
## Usage
You can locate yourself by issuing ```/locate```. The result is broadcasted on the server.
In this screenshot you can see me (ByNetherdude) locating myself.
![Locate Self](img/locate_self.PNG)

You can locate other players by issuing ```/locate <name>```.
In this screenshot you can see me (ByNetherdude) locating my 2nd account (spurlos).
![Locate Self](img/locate_other.PNG)


## Contact
Found a bug or need a new feature?
Contact me:

- Discord: ByNetherdude#8846
- E-Mail: bynetherdude@gmail.com

Alternatively you can also create a Gitlab Issue.
