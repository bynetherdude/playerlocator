package me.bynetherdude.playerlocator;

import org.bukkit.plugin.java.JavaPlugin;

public final class PlayerLocator extends JavaPlugin {

    @Override
    public void onEnable() {
       System.out.println("PlayerLocator started.");
       commandRegistration();
    }

    private void commandRegistration() {
        getCommand("locate").setExecutor(new LocateCommand());
    }
}
