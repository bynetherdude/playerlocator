package me.bynetherdude.playerlocator;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LocateCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        // Exit early if arg length invalid
        if (!(args.length == 0 || args.length == 1)) {
            sender.sendMessage("§7Invalid arguments, too long or too short.");
            return true;
        }

        if (args.length == 0) {
            if (sender instanceof Player) {
                Player s = (Player) sender;
                int x = (int) s.getLocation().getX();
                int y = (int) s.getLocation().getY();
                int z = (int) s.getLocation().getZ();
                Bukkit.broadcastMessage("§7Player " + s.getName() + " is at: §c" + x + "§7, §c" + y + "§7, §c" + z + "§7.");
            } else {
                sender.sendMessage("§7Command can only be executed as a player.");
            }
            return true;
        }

        // Args lenght must now be 1
        // Define player, exit if invalid
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            sender.sendMessage("§7Invalid arguments, Player not online.");
            return true;
        }

        if (target.isOnline()) {
            int x = (int) target.getLocation().getX();
            int y = (int) target.getLocation().getY();
            int z = (int) target.getLocation().getZ();
            sender.sendMessage("§7Player " + target.getName() + " is at: §c" + x + "§7, §c" + y + "§7, §c" + z + "§7.");
        }
        return true;
    }
}
